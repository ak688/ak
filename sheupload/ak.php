﻿<?php
//require("yz.php");//用session验证用户是否登录，否则跳转登录页
require("conn.php");
$num = mysqli_query($conn,"SELECT * FROM `time` ");//对应conn.php里的 $conn
$row=mysqli_fetch_all($num,MYSQLI_ASSOC);
?>

<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title>首页</title>
		<link rel="stylesheet" href="css/layui.css" />
		<script type="text/javascript" src="js/jquery.min.js" ></script>
		<script type="text/javascript" src="js/index.js" ></script>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<style type="text/css">

body,td,th,table {
	font-family: 幼圆;
	font-size: 20px;
}
body {
	background-color: #FFFFFF;


}
body {
	margin: 0px;
	padding: 0px;
}

.left {
	background: #313541;
	width: 15%;
	float: inherit;
	position: absolute;
	top: -2px;
	bottom: 0px;
	left: 0px;
}

.top {
	width: 85%;
	float: right;
	height: 100px;
	line-height: 100px;
	border-bottom: 1px solid #b0cdff;
}

.content {
	float:right;
	width: 85%;
	text-align: center;
	font-size: 20px;
	margin-top: 10px;
}
.content td{ text-align:center;}

.leftTiyle {
	font-size: 25px;
	padding-left:0px;
	font-weight: 600;
	color: #545454;
	float: left;
}

.thisUser {
	float: right;
	margin-right: 30px;
}

.thisUser1 {
	float: right;
	margin-right: 10px;
}

.bigTitle {
	background: #3d598a;
	color: #fff;
	height: 100px;
	line-height: 100px;
	text-align: center;
	font-size: 24px;
	font-family: -webkit-body;
}

.lines .active {
	background: #272a34;
	color: #fff;
}
.lines img{
    width: 23px;
    vertical-align: middle;
    margin-bottom: 4px;
    margin-right: 9px;
}
.lines div {
	height: 70px;
	line-height: 70px;
	padding-left: 50px;
	color: #707783;
}

.lines div:hover {
	cursor: pointer;
	color: #FFFFFF;
}

</style>

    <script>
        // jQuery代码
        $(document).ready(function () {
		    $("#ak1").show();// 当页面第一次加载，只显示ak1 2019.10.26 01：52 ak
			$("#ak2").hide();
			$("#ak3").hide();
            $("#b1").click(function () {
                $("#ak1").show();// 显示
                $("#ak2").hide();// 隐藏
				$("#ak3").hide();
            });
            $("#b2").click(function () {
                $("#ak1").hide();
                $("#ak2").show();
				$("#ak3").hide();
            });
			
			$("#b3").click(function () {
                $("#ak1").hide();
				$("#ak2").hide();
                $("#ak3").show();
            });
			
        });
    </script>
</head> 
<body>	

		<div class="left">
			<div class="bigTitle">商店后台管理系统</div>
			<div class="lines">
				<div onClick="pageClick(this)" class="active" id="b1"><img src="img/icon-1.png" />登陆</div>
				
				<div onClick="pageClick(this)" id="b3"><img src="img/icon-3.png" />执行</div>
</div>
		</div>

		<div class="top">
		  <div class="leftTiyle" id="flTitle">当前出售管理</div>
			<div class="thisUser"></div>
			<div class="thisUser"></div>
		</div>
		


		
		<div class="content" id="ak1">
		 <table width="100%" border="1" cellspacing="1" cellpadding="1" height="95" class="layui-table">
         <thead>
			<tr>
			  <td width="2%" height="">ID</td>
			  
			  <td width="20%">用户名</td>
			  <td width="10%">登陆时间</td>
			  <td width="35%">IP</td>
			  <td width="20%">是否成功</td>
			  
		    </tr>
		</thead>
            <?php foreach ($row as $num => $info) { ?> <!-- //主要语句，解决查询数据库只输出一个数据得问题    ak袁锟铨 2019.10.26 01:28-->
            <tr>
              <td height="63" data-label="ID"><?= $info["id"]  ?></td>
			  
              <td data-label="菜名"><?= $info["user"]  ?></td>
              <td data-label="价格"><?= $info["time"]   ?></td>
              <td data-label="介绍"><?= $info["ip"]   ?></td>
              <td data-label="介绍"><?= $info["login"]   ?></td>
             
            </tr>
			
            <?php } ?>   <!-- //主要语句，解决查询数据库只输出一个数据得问题-->
          </table>
		</div>
		
		
		  <div class="content" id="ak3">
<?php
require("conn.php");
$num = mysqli_query($conn,"SELECT * FROM `search`");
$row=mysqli_fetch_all($num,MYSQLI_ASSOC);
?>
		  <table width="100%" border="1" cellspacing="1" cellpadding="1" height="95" class="layui-table">
         <thead>
			<tr>
			  <td width="30" height="">ID</td>
			  <td width="195">时间</td>
			  <td width="228">IP</td>
			  <td width="283">执行</td>
		    </tr>
		</thead>
            <?php foreach ($row as $num => $info) { ?> <!-- //主要语句，解决查询数据库只输出一个数据得问题    ak袁锟铨 2019.10.26 01:28-->
            <tr>
              <td height="63" data-label="ID"><?= $info["id"]  ?></td>
              <td data-label="菜名"><?= $info["time"]  ?></td>
              <td data-label="价格"><?= $info["ip"]   ?></td>
              <td data-label="介绍"><?= $info["type"]   ?></td>
             
            </tr>
			
            <?php } ?>   <!-- //主要语句，解决查询数据库只输出一个数据得问题-->
          </table>	  
		  </div>
		  
		  
		  <div>
		  
		  </div>

</body>
</html>