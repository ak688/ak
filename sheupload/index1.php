﻿<?php
require("yz.php");//用session验证用户是否登录，否则跳转登录页
?>	
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title>首页</title>
		<link rel="stylesheet" href="css/page.css" />
		<script type="text/javascript" src="js/jquery.min.js" ></script>
		<script type="text/javascript" src="js/index.js" ></script>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<style type="text/css">

body,td,th {
	font-family: 幼圆;
	font-size: 23px;
}
body {
	background-color: #FFFFFF;
}
body {
	margin: 0px;
	padding: 0px;
}

.left {
	background: #313541;
	width: 15%;
	float: inherit;
	position: absolute;
	top: -2px;
	bottom: 0px;
	left: 0px;
}

.top {
	width: 85%;
	float: right;
	height: 100px;
	line-height: 100px;
	border-bottom: 1px solid #b0cdff;
}

.content {
	float:right;
	width: 85%;
	text-align: center;
	font-size: 20px;
	margin-top: 10px;
}
.content td{ text-align:center;}

.leftTiyle {
	font-size: 18px;
	padding-left:0px;
	font-weight: 600;
	color: #545454;
	float: left;
}

.thisUser {
	float: right;
	margin-right: 30px;
}

.bigTitle {
	background: #3d598a;
	color: #fff;
	height: 100px;
	line-height: 100px;
	text-align: center;
	font-size: 20px;
	font-family: -webkit-body;
}
.lines{
	font-size: 16px;
}

.lines .active {
	background: #272a34;
	color: #fff;
}
.lines img{
    width: 23px;
    vertical-align: middle;
    margin-bottom: 4px;
    margin-right: 9px;
}
.lines div {
	height: 70px;
	line-height: 70px;
	padding-left: 20px;
	color: #707783;
}

.lines div:hover {
	cursor: pointer;
	color: #FFFFFF;
}

</style>

    <script>
        // jQuery代码
        $(document).ready(function () {
		    $("#a1").show();// 当页面第一次加载，只显示a1 2019.10.26 01：52 
			$("#a2").hide();
			$("#a3").hide();
			$("#a4").hide();
			$("#a5").hide();
			$("#a6").hide();
			$("#a7").hide();
            $("#b1").click(function () {
                $("#a1").show();// 显示
                $("#a2").hide();// 隐藏
				$("#a3").hide();
				$("#a4").hide();
				$("#a5").hide();
				$("#a6").hide();
				$("#a7").hide();
            });
            $("#b2").click(function () {
                $("#a1").hide();
                $("#a2").show();
				$("#a3").hide();
				$("#a4").hide();
				$("#a5").hide();
				$("#a6").hide();
				$("#a7").hide();
            });
			
			$("#b3").click(function () {
                $("#a1").hide();
				$("#a2").hide();
                $("#a3").show();
				$("#a4").hide();
				$("#a5").hide();
				$("#a6").hide();
				$("#a7").hide();
            });
			
			$("#b4").click(function () {
                $("#a1").hide();
				$("#a2").hide();
				$("#a3").hide();
                $("#a4").show();
				$("#a5").hide();
				$("#a6").hide();
				$("#a7").hide();
            });
			$("#b5").click(function () {
                $("#a1").hide();
				$("#a2").hide();
				$("#a3").hide();
				$("#a4").hide();
				$("#a5").show();
				$("#a6").hide();
				$("#a7").hide();
            });
			$("#b5").click(function () {
                $("#a1").hide();
				$("#a2").hide();
				$("#a3").hide();
				$("#a4").hide();
				$("#a5").show();
				$("#a6").hide();
				$("#a7").hide();
            });
			$("#b6").click(function () {
                $("#a1").hide();
				$("#a2").hide();
				$("#a3").hide();
				$("#a4").hide();
				$("#a5").hide();
				$("#a6").show();
				
				$("#a7").hide();
            });
			$("#b7").click(function () {
                $("#a1").hide();
				$("#a2").hide();
				$("#a3").hide();
				$("#a4").hide();
				$("#a5").hide();
				$("#a6").hide();
				$("#a7").show();			
            });
        });
    </script>
</head> 
<body>	

		<div class="left">
			<div class="bigTitle">商店后台管理系统</div>
			<div class="lines">
				<div onClick="pageClick(this)" class="active" id="b1"><img src="img/icon-1.png" />当前出售管理</div>
				<div onClick="pageClick(this)" id="b2"><img src="img/icon-2.png" />上传新增菜品</div>
				<div onClick="pageClick(this)" id="b3"><img src="img/icon-3.png" />上架菜品</div>
				<div onClick="pageClick(this)" id="b4"><img src="img/icon-4.png" />荤食系列图片</div>
				<div onClick="pageClick(this)" id="b5"><img src="img/icon-5.png" />蔬菜瓜谷系列图片</div>
				<div onClick="pageClick(this)" id="b6"><img src="img/icon-5.png" />缤纷水果系列图片</div>
			  <div onClick="pageClick(this)" id="b7"><img src="img/icon-5.png" />酱汁系列图片</div>
			</div>
		</div>
		
		
		<div class="top">
			<div class="leftTiyle" id="flTitle">当前出售管理</div>
			<div class="thisUser"><form action="loginout.php"  method="post"><input type=submit value="退出" /></form></div>
			<div class="thisUser">当前用户：<?=$_SESSION["username"]?></div>
		</div>
		


		
		<div class="content" id="a1">
		 <table width="100%" border="1" cellspacing="1" cellpadding="1" height="95">
         <thead>
			<tr>
			  <td width="30" height="">ID</td>
			  <td width="195">菜名</td>
			  <td width="228">价格</td>
			  <td width="283">介绍</td>
			  <td width="146">更多</td>
			  <td width="146">图片</td>
			  <td colspan="3">修改</td>
		    </tr>
		</thead>
	
		
<!--连接数据库并查询-->		
<?php
require("conn.php");			 
$num = mysqli_query($conn,"SELECT * FROM `a` WHERE `xianshi` = 0");   //执行查询  从表 a 中搜索显示（xianshi=0）的数据（即出售中的商品）。设计时，默认0为显示状态，则上传新菜品是 写入的状态也为0 ，，，   以此实现上下架功能    			 
$row=mysqli_fetch_all($num,MYSQLI_ASSOC);   //从结果集中取得所有作为关联数组
?>    
       <!--foreach循环遍历，将以上取得的数据 全部循环遍历给$info-->    
       <?php foreach ($row as $num => $info) { ?> <!-- //主要语句，解决查询数据库只输出一行数据的问题     2019.10.26 01:28   在此遍历输出所有查询的数据-->
            <tr>
              <td height="63" data-label="ID"><?= $info["id"]  ?></td>
              <td data-label="菜名"><?= $info["name"]  ?></td>
              <td data-label="价格"><?= $info["jiage"]   ?></td>
              <td data-label="介绍"><?= $info["jieshao"]   ?></td>
              <td data-label="介绍"><?= $info["more"]   ?></td>
              <td><?= "<img src=".$info["tupian"]." width='100%' height='80px'>";   ?></td>
              
              
              <td width="65"><form action="down.php"  method="post"><input type=hidden name=id value=<?= $info["id"]  ?> /><input type=submit value="下架" /></form></td> </td>
              
              
			  <td width="65"><form action="modify.php"  method="post"><input type=hidden name=id value=<?= $info["id"]  ?> /><input type=submit value="修改" /></form></td> </td>	
		  	  
			  	  
			  <!--<td><button type="button" onClick="if(confirm('确认删除吗')) window.location.href='del.php'; else return false;">删除</button> </td>--> 
			  <td width="65"><form action="del.php"  method="post"><input type=hidden name=id value=<?= $info["id"]  ?> /><input type=submit value="删除" /></form></td>      <!--2019.10.27 20：06 以上id可替换成name，那么提示框中可输出菜名-->
            </tr>
			
       <?php } ?>   <!-- //主要语句，解决查询数据库只输出一个数据得问题-->
          </table>
		</div>
		
		
		<!--上传新菜品页部分-->
		<div class="content"id="a2">
		<form action="shangchuan.php" method="post" enctype="multipart/form-data" onSubmit="return InputCheck(this)"><!-- enctype="multipart/form-data" onSubmit="return InputCheck(this)" 上传图片必须-->
		
		<table width="497" height="400">
				  <tr>
					<td width="102">菜名</td>
					<td width="100">
					  <label>
					    <input type="text" name="ak3">
				      </label>					</td>
				  </tr>
				  <tr>
					<td>价格</td>
					<td>
					  <label>
					    <input type="text" name="ak4">
				      </label>					</td>
				  </tr>
				  <tr>
					<td>介绍</td>
					<td>
					  <label>
					    <input type="text" name="ak5">
				      </label>					</td>
				  </tr>

				  <tr>
					<td>详情</td>
					<td>
					  <label></label>
                      <textarea type="text" height="300" width="200" name="ak6"></textarea></td>
				  </tr>

				  <tr>
					<td>图片</td>
					<td>
					　　<label><input type="file" name="file" id="file"></label><br>					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>
					  <label>
					    <input type="submit" name="Submit" value="提交">
				      </label>					</td>
				  </tr>
          </table>	
		  </form>
          </div>
		  
		  
		  
		  <!--上架菜品页-->
		  <div class="content" id="a3">
	  
<?php
require("conn.php");
$num = mysqli_query($conn,"SELECT * FROM `a` WHERE `xianshi` = 1");   //执行查询  从表 a 中搜索显示（xianshi=1）的数据（即已下架的商品）。设计时，默认0为显示状态，则上传新菜品是 写入的状态也为0 ，，，   以此实现上下架功能    
$row=mysqli_fetch_all($num,MYSQLI_ASSOC);
?>




		  <table width="100%" border="1" cellspacing="1" cellpadding="1" height="95">
         <thead>
			<tr>
			  <td width="30" height="">ID</td>
			  <td width="195">菜名</td>
			  <td width="228">价格</td>
			  <td width="283">介绍</td>
			  <td width="146">详情</td>
			  <td width="146">图片</td>
			  <td colspan="2">修改</td>
		    </tr>
		</thead>
		
		    <!--foreach循环遍历，将以上取得的数据 全部循环遍历给$info-->
            <?php foreach ($row as $num => $info) { ?> <!-- //主要语句，解决查询数据库只输出一个数据得问题   2019.10.26 01:28-->
            <tr>
              <td height="63" data-label="ID"><?= $info["id"]  ?></td>
              <td data-label="菜名"><?= $info["name"]  ?></td>
              <td data-label="价格"><?= $info["jiage"]   ?></td>
              <td data-label="介绍"><?= $info["jieshao"]   ?></td>
              <td data-label="介绍"><?= $info["more"]   ?></td>
              <td><?= "<img src=".$info["tupian"]." width='100%' height='80px'>";   ?></td>
              <td width="65"><form action="up.php"  method="post"><input type=hidden name=id value=<?= $info["id"]  ?> /><input type=submit value="上架" /></form></td> </td>
			  <!--<td><button type="button" onClick="if(confirm('确认删除吗')) window.location.href='del.php'; else return false;">删除</button> </td>--> 
			  
			  <td width="65"><form action="del.php"  method="post"><input type=hidden name=id value=<?= $info["id"]  ?> /><input type=submit value="删除" /></form></td>      <!--2019.10.27 20：06 ak阿锟 以上id可替换成name，那么提示框中可输出菜名-->
            </tr>
			
            <?php } ?>   <!-- //主要语句，解决查询数据库只输出一个数据得问题-->
          </table>	  
		  </div>
		  
		  
		  <div id="a4" class="content">
		  
<?php
require("conn.php");
$num = mysqli_query($conn,"SELECT * FROM `b`");   //执行查询  从表 a 中搜索显示（xianshi=1）的数据（即已下架的商品）。设计时，默认0为显示状态，则上传新菜品是 写入的状态也为0 ，，，   以此实现上下架功能    
$row=mysqli_fetch_all($num,MYSQLI_ASSOC);
?>
		  
			  <!--<form action="shangchuan.php" method="post" enctype="multipart/form-data" onSubmit="return InputCheck(this)"> enctype="multipart/form-data" onSubmit="return InputCheck(this)" 上传图片必须-->
			
			  <form method="post" action="upload.php" enctype="multipart/form-data" onSubmit="return InputCheck(this)">
			  	   <!--<input type="radio" name="cho" value="单点自选系列" title="单点自选系列">单点自选系列
                   <input type="radio" name="cho" value="沙拉套餐系列" title="沙拉套餐系列">沙拉套餐系列
                   <input type="radio" name="cho" value="酱汁系列" title="酱汁系列" checked>酱汁系列
                   <input type="radio" name="" value="" title="上传前先选择" disabled>上传前先选择-->
			       <input name='file' type="file">
			       <input type="submit" name="file" value="上传">
              </form>
			  <br>
	    <table width="100%" border="1" cellspacing="1" cellpadding="1" height="80">
         <thead>
			<tr>
			  <td width="30" height="">ID</td>
			  <td width="146">图片</td>
			  <td width="30">删除</td>
		    </tr>
		</thead>
		
		    <!--foreach循环遍历，将以上取得的数据 全部循环遍历给$info-->
            <?php foreach ($row as $num => $info) { ?> <!-- //主要语句，解决查询数据库只输出一个数据得问题   2019.10.26 01:28-->
            <tr>
			  <td height="63" data-label="ID"><?= $info["id"]  ?></td>
              <td><?= "<img src=".$info["tupian"]." width='40%' height='120px'>";   ?></td>
             
			  
			  <td width="65"><form action=""  method="post"><input type=hidden name=id value=<?= $info["id"]  ?> /><input type=submit value="删除" /></form></td>
            </tr>
			
            <?php } ?>   <!-- //主要语句，解决查询数据库只输出一个数据得问题-->
          </table>
		  
		  </div>
		  
		  
		  		  
		  <div id="a5" class="content">
<?php
require("conn.php");
$num = mysqli_query($conn,"SELECT * FROM `c`");   //执行查询  从表 a 中搜索显示（xianshi=1）的数据（即已下架的商品）。设计时，默认0为显示状态，则上传新菜品是 写入的状态也为0 ，，，   以此实现上下架功能    
$row=mysqli_fetch_all($num,MYSQLI_ASSOC);
?>
			  <!--<form action="shangchuan.php" method="post" enctype="multipart/form-data" onSubmit="return InputCheck(this)"> enctype="multipart/form-data" onSubmit="return InputCheck(this)" 上传图片必须-->
			
			  <form method="post" action="upload1.php" enctype="multipart/form-data" onSubmit="return InputCheck(this)">
			       <input name='file' type="file">
			       <input type="submit" name="file" value="上传">
              </form><br>
	       <table width="100%" border="1" cellspacing="1" cellpadding="1" height="80">
         <thead>
			<tr>
			  <td width="30" height="">ID</td>
			  <td width="146">图片</td>
			  <td width="30">删除</td>
		    </tr>
		</thead>
		
		    <!--foreach循环遍历，将以上取得的数据 全部循环遍历给$info-->
            <?php foreach ($row as $num => $info) { ?> <!-- //主要语句，解决查询数据库只输出一个数据得问题   2019.10.26 01:28-->
            <tr>
			  <td height="63" data-label="ID"><?= $info["id"]  ?></td>
              <td><?= "<img src=".$info["tupian"]." width='40%' height='120px'>";   ?></td>
             
			  
			  <td width="65"><form action=""  method="post"><input type=hidden name=id value=<?= $info["id"]  ?> /><input type=submit value="删除" /></form></td>
            </tr>
			
            <?php } ?>   <!-- //主要语句，解决查询数据库只输出一个数据得问题-->
          </table>
		  
		  </div>
		  
		  <div id="a6" class="content">
<?php
require("conn.php");
$num = mysqli_query($conn,"SELECT * FROM `d`");   //执行查询  从表 a 中搜索显示（xianshi=1）的数据（即已下架的商品）。设计时，默认0为显示状态，则上传新菜品是 写入的状态也为0 ，，，   以此实现上下架功能    
$row=mysqli_fetch_all($num,MYSQLI_ASSOC);
?>
			  <!--<form action="shangchuan.php" method="post" enctype="multipart/form-data" onSubmit="return InputCheck(this)"> enctype="multipart/form-data" onSubmit="return InputCheck(this)" 上传图片必须-->
			
			  <form method="post" action="upload2.php" enctype="multipart/form-data" onSubmit="return InputCheck(this)">
			       <input name='file' type="file">
			       <input type="submit" name="file" value="上传">
              </form><br>
	       <table width="100%" border="1" cellspacing="1" cellpadding="1" height="80">
         <thead>
			<tr>
			  <td width="30" height="">ID</td>
			  <td width="146">图片</td>
			  <td width="30">删除</td>
		    </tr>
		</thead>
		
		    <!--foreach循环遍历，将以上取得的数据 全部循环遍历给$info-->
            <?php foreach ($row as $num => $info) { ?> <!-- //主要语句，解决查询数据库只输出一个数据得问题   2019.10.26 01:28-->
            <tr>
			  <td height="63" data-label="ID"><?= $info["id"]  ?></td>
              <td><?= "<img src=".$info["tupian"]." width='40%' height='120px'>";   ?></td>
             
			  
			  <td width="65"><form action=""  method="post"><input type=hidden name=id value=<?= $info["id"]  ?> /><input type=submit value="删除" /></form></td>
            </tr>
			
            <?php } ?>   <!-- //主要语句，解决查询数据库只输出一个数据得问题-->
          </table>
		  </div>
		  
		  
		  <div id="a7" class="content">
<?php
require("conn.php");
$num = mysqli_query($conn,"SELECT * FROM `e`");   //执行查询  从表 a 中搜索显示（xianshi=1）的数据（即已下架的商品）。设计时，默认0为显示状态，则上传新菜品是 写入的状态也为0 ，，，   以此实现上下架功能    
$row=mysqli_fetch_all($num,MYSQLI_ASSOC);
?>
			  <!--<form action="shangchuan.php" method="post" enctype="multipart/form-data" onSubmit="return InputCheck(this)"> enctype="multipart/form-data" onSubmit="return InputCheck(this)" 上传图片必须-->
			
			  <form method="post" action="upload3.php" enctype="multipart/form-data" onSubmit="return InputCheck(this)">
			       <input name='file' type="file">
			       <input type="submit" name="file" value="上传">
              </form><br>
	       <table width="100%" border="1" cellspacing="1" cellpadding="1" height="80">
         <thead>
			<tr>
			  <td width="30" height="">ID</td>
			  <td width="146">图片</td>
			  <td width="30">删除</td>
		    </tr>
		</thead>
		
		    <!--foreach循环遍历，将以上取得的数据 全部循环遍历给$info-->
            <?php foreach ($row as $num => $info) { ?> <!-- //主要语句，解决查询数据库只输出一个数据得问题   2019.10.26 01:28-->
            <tr>
			  <td height="63" data-label="ID"><?= $info["id"]  ?></td>
              <td><?= "<img src=".$info["tupian"]." width='40%' height='120px'>";   ?></td>
             
			  
			  <td width="65"><form action=""  method="post"><input type=hidden name=id value=<?= $info["id"]  ?> /><input type=submit value="删除" /></form></td>
            </tr>
			
            <?php } ?>   <!-- //主要语句，解决查询数据库只输出一个数据得问题-->
          </table>
		  </div>

</body>
</html>